require 'test/unit'
require './leap-year.rb'
class TestPractice < Test::Unit::TestCase
  def setup
    @leap = Leap.new
  end
  def test_leap_year_when_true
    assert_equal("It is a leap year", @leap.leap_year(2012))
  end
  def test_leap_year_when_false
    assert_equal("It is not a leap year", @leap.leap_year(2200))
  end
  def test_leap_when_not_number
    assert_equal("Sorry it is not a year", @leap.leap_year('hey'))
  end
end
