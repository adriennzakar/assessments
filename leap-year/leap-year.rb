class Leap
  def leap_year(x)
    if x.is_a? Integer
      if (x % 4 == 0 && x % 100 != 0) || (x % 400 == 0)
        return "It is a leap year"
      else
        return "It is not a leap year"
      end
    else
      return "Sorry it is not a year"
    end
  end
end
