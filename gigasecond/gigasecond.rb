class GigaSec
    minute_in_sec = 60
    hour_in_sec = minute_in_sec * 60
    day_in_sec = hour_in_sec * 24
    week_in_sec = day_in_sec * 7
    year_in_sec = 365 * day_in_sec
    gigasec = 1000000000
    years = gigasec / year_in_sec
    weeks = gigasec % year_in_sec / week_in_sec
    days = gigasec % year_in_sec % week_in_sec / day_in_sec
    hours = gigasec % year_in_sec % week_in_sec % day_in_sec / hour_in_sec
    minutes = gigasec % year_in_sec % week_in_sec % day_in_sec % hour_in_sec / minute_in_sec
    seconds = gigasec % year_in_sec % week_in_sec % day_in_sec % hour_in_sec % minute_in_sec
    print "Someone who lived for a gigasecond is #{years} years #{weeks} weeks #{days} days #{hours} hours #{minutes} minutes and #{seconds} seconds old at that moment."
end
